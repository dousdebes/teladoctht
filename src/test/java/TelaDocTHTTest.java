/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ar.com.teladoc.SnakeSnail;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Alberto Dousdebes
 */
public class TelaDocTHTTest {
    
    public TelaDocTHTTest() {
    }
    
    @Test
    public void testInputString() {
        String inputString = "a";
        assertArrayEquals(new Integer[0][0], SnakeSnail.createSnailShell(inputString));
    }
    
    @Test
    public void testInputNegative() {
        Integer negativeInput = -9;
        assertArrayEquals(new Integer[0][0], SnakeSnail.createSnailShell(negativeInput));
    }
    
    @Test
    public void testN3Matrix() {
        Integer baseNumber = 3;
        Integer[][] expected = {{1, 2, 3}, {8,9,4}, {7,6,5}};
        assertArrayEquals(expected, SnakeSnail.createSnailShell(baseNumber));
    }
    
   
    @Test
    public void testN4Matrix() {
        Integer baseNumber = 4;
        Integer[][] expected = {{1,2,3,4}, {12, 13, 14, 5}, {11, 16, 15, 6}, {10, 9, 8, 7}};
        assertArrayEquals(expected, SnakeSnail.createSnailShell(baseNumber));
    }
    
     @Test
    public void testN5Matrix() {
        Integer baseNumber = 5;
        Integer[][] expected = {{1, 2, 3, 4, 5}, {16, 17, 18, 19, 6}, {15, 24, 25, 20, 7}, {14, 23, 22, 21, 8} , {13, 12, 11, 10, 9}};
        assertArrayEquals(expected, SnakeSnail.createSnailShell(baseNumber));
    }
    
}
