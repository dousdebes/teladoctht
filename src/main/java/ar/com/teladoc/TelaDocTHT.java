/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.teladoc;

import java.util.Arrays;

/**
 *
 * @author Alberto
 */
public class TelaDocTHT {

    static final int BASE_NUMBER = 3;
    static final String BASE_NUMBER_STRING = "8";
    static final int BASE_NUMBER_NEG = -4;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Integer[][] resultMatrix = SnakeSnail.createSnailShell(BASE_NUMBER);
        System.out.println("Output: " + Arrays.deepToString(resultMatrix));
    }

}
