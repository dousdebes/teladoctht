/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.teladoc;

import java.util.Arrays;

/**
 *
 * @author Alberto
 */
public class SnakeSnail {
    
    
    public static Integer[][] createSnailShell(Object baseNumberInput) {
        
        Integer baseNumber;
        
        //validating the input
        if (baseNumberInput instanceof Integer ) {
            baseNumber = (Integer) baseNumberInput;
            if (baseNumber < 1) {
                return new Integer[0][0];
            }
        } else {
            return new Integer[0][0];
        }
        
        
        //Initializing main array
        Integer[][] matrix = new Integer[baseNumber][baseNumber];
        
        Integer counter = 0;
        int currentArray = 0;
        while (counter < Math.pow(baseNumber, 2)) {
            //fill the first array (return last inserted value)
            counter = fillArrayClockwise(baseNumber, counter, matrix[currentArray]);
            //take the last empty element of the remaining arrays and fill it
            currentArray += 1;
            for (int i = currentArray; i < baseNumber; i++) {
                if (getLastEmptyElement(baseNumber, matrix[i]) != -1) {
                    counter += 1;
                    matrix[i][getLastEmptyElement(baseNumber, matrix[i])] = counter;
                }
                currentArray = i;
            }

            //fill the current array in reverse order
            //if the array if full then decrement the index
            if (!Arrays.asList(matrix[currentArray]).contains(null)) {
                currentArray -= 1;
            }
            counter = fillArrayCounterClockwise(baseNumber, counter, matrix[currentArray]);

            //take the first empty element of the remaining arrays and fill it
            for (int i = currentArray - 1; i >= 0; i--) {
                if (getFirstEmptyElement(baseNumber, matrix[i]) != -1) {
                    counter += 1;
                    matrix[i][getFirstEmptyElement(baseNumber, matrix[i])] = counter;
                }
                currentArray = i;
            }

            currentArray += 1;
        }
        
        return matrix; 
        
    }
    
    public static int getLastEmptyElement(Integer baseNumber, Integer[] array) {
        for (int i = baseNumber - 1; i >= 0; i--) {
            if (array[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public static int getFirstEmptyElement(Integer baseNumber, Integer[] array) {
        for (int i = 0; i < baseNumber; i++) {
            if (array[i] == null) {
                return i;
            }
        }
        return -1;
    }

    public static Integer fillArrayClockwise(Integer baseNumber, Integer counter, Integer[] array) {
        for (int i = 0; i < baseNumber; i++) {
            if (array[i] == null) {
                counter += 1;
                array[i] = counter;

            }
        }
        return counter;
    }

    public static Integer fillArrayCounterClockwise(Integer baseNumber, Integer counter, Integer[] array) {
        for (int i = baseNumber - 1; i >= 0; i--) {
            if (array[i] == null) {
                counter += 1;
                array[i] = counter;
            }
        }
        return counter;
    }
}
