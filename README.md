# TelaDoc Take Home Test for Alberto A. Dousdebes


## Project Overview

The main objective of this project is to create a program that generate a NxN matrix that resembles the snail shell. 

The core class is `SnakeSnail.java` where it's defined the method `createSnailShell`. 

There are some test defined in the `test` package in the class `TelaDocTHTTest.java`

It's developed using Java. 

## Enviroment and Frameworks

* JDK 1.8
* Netbeans 18
* Maven
* JUnit
